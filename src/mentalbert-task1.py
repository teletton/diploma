from numpy.random.mtrand import random
import pandas as pd
import numpy as np
from nlp import Dataset
from transformers import AutoTokenizer
from datasets import load_metric
from transformers import TrainingArguments
from transformers import Trainer
from transformers import AutoModelForSequenceClassification
from transformers import TrainingArguments
from transformers import DataCollatorWithPadding
from datasets import load_metric
#import evaluate
import os
os.environ['CUDA_VISIBLE_DEVICES']="6"


tokenizer = AutoTokenizer.from_pretrained("mental/mental-bert-base-uncased")
def preprocess_function(examples):
    return tokenizer(examples["text"],truncation=True)

    



train_df = pd.read_csv("./data/reddit_data/train.csv")
test_df = pd.read_csv("./data/reddit_data/test.csv")
val_df = pd.read_csv("./data/reddit_data/val.csv")


preds = pd.DataFrame()
from sklearn.utils import shuffle
train_df = shuffle(train_df)
val_df = shuffle(val_df)
test_df = shuffle(test_df)
train_df = pd.concat([train_df, val_df], axis=0)

acc = []
f1 = []
idx = []

# rename dataset


train_ds = Dataset.from_pandas(train_df[["text", "label"]])
val_ds = Dataset.from_pandas(val_df[["text", "label"]])
test_ds = Dataset.from_pandas(test_df[["text", "label"]])
#tokenize
tokenized_train = train_ds.map(preprocess_function)
tokenized_val = val_ds.map(preprocess_function)
tokenized_test = test_ds.map(preprocess_function)

tokenized_train.drop('text')
tokenized_val.drop('text')
tokenized_test.drop('text')



data_collator = DataCollatorWithPadding(tokenizer=tokenizer)



i = 3
model = AutoModelForSequenceClassification.from_pretrained("mental/mental-bert-base-uncased", num_labels=3)
training_args = TrainingArguments(
    output_dir=f'./results/mentalbert/mental-task1-exp-{i}',
    per_device_train_batch_size=32,
    num_train_epochs=15,
    learning_rate=0.0001
)

# Trainer object initialization
trainer = Trainer(
    model=model,
    args=training_args,
    train_dataset=tokenized_train,
    tokenizer=tokenizer,
    data_collator=data_collator
)

trainer.train()

pred = trainer.predict(tokenized_val)

model_predictions = np.argmax(pred[0], axis=1)

model.save_pretrained(f"./models_new/mentalbert/mental-task1-exp-{i}")

from scipy.special import softmax

pred = trainer.predict(tokenized_test)
pred_train = trainer.predict(tokenized_train)


preds_test = pd.DataFrame(np.concatenate([softmax(pred[0], axis=1), pred[1].reshape((-1, 1))], axis=1), columns=['0', '1', '2', 'label'])
preds_train = pd.DataFrame(np.concatenate([softmax(pred_train[0], axis=1), pred_train[1].reshape((-1, 1))], axis=1), columns=['0', '1', '2', 'label'])
preds_test.to_csv(f"./predictions_new/mentalbert/mental-task1-exp-{i}-preds-test.csv", index=False)
preds_train.to_csv(f"./predictions_new/mentalbert/mental-task1-exp-{i}-preds-train.csv", index=False)
model_predictions = np.argmax(softmax(pred[0], axis=1), axis=1)
preds["label"] = model_predictions

metric = load_metric("accuracy")
metric_f1 = load_metric("f1")
metric_precision = load_metric("precision")
metric_recall = load_metric("recall")

preds.to_csv(f"./predictions_new/mentalbert/mental-task1-exp-{i}-preds.csv", index=False)

final_score = metric.compute(predictions=model_predictions, references=tokenized_test['label'])
print(final_score)
final_score = metric_f1.compute(predictions=model_predictions, references=tokenized_test['label'], average="weighted")
print(final_score)




