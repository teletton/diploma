from numpy.random.mtrand import random
import pandas as pd
import numpy as np
from nlp import Dataset
from transformers import AutoTokenizer
from datasets import load_metric
from transformers import TrainingArguments
from transformers import Trainer
from transformers import AutoModelForSequenceClassification
from transformers import TrainingArguments
from transformers import DataCollatorWithPadding
from datasets import load_metric
#import evaluate
import os
os.environ['CUDA_VISIBLE_DEVICES']="0"


tokenizer = AutoTokenizer.from_pretrained("mental/mental-bert-base-uncased")
def preprocess_function(examples):
    return tokenizer(examples["text"],truncation=True)

    



train_df = pd.read_csv("./data/twitter_data/df_train.csv")
test_df = pd.read_csv("./data/twitter_data/df_test.csv")
val_df = pd.read_csv("./data/twitter_data/df_val.csv")


train_df = train_df.rename(columns={
    "Text" : "text", 
    "Sentiment" : "label"
})
test_df = test_df.rename(columns={
    "Text" : "text", 
    "Sentiment" : "label"
})

val_df = val_df.rename(columns={
    "Text" : "text", 
    "Sentiment" : "label"
})


#train_df = pd.concat([train_df, val_df], axis=0)
#preds = pd.DataFrame()
train_df["label"] = train_df["label"].astype(int)
test_df["label"] = test_df["label"].astype(int)
val_df["label"] = val_df["label"].astype(int)

train_df.loc[train_df["label"] == 2, "label"] = int(1)
train_df.loc[train_df["label"] == 3, "label"] = int(2)

val_df.loc[val_df["label"] == 2, "label"] = int(1)
val_df.loc[val_df["label"] == 3, "label"] = int(2)

test_df.loc[test_df["label"] == 2, "label"] = int(1)
test_df.loc[test_df["label"] == 3, "label"] = int(2)

preds = pd.DataFrame()

train_df = pd.concat([train_df, val_df], axis=0)

acc = []
f1 = []
idx = []

# rename dataset


train_ds = Dataset.from_pandas(train_df[["text", "label"]])
val_ds = Dataset.from_pandas(val_df[["text", "label"]])
test_ds = Dataset.from_pandas(test_df[["text", "label"]])
#tokenize
tokenized_train = train_ds.map(preprocess_function)
tokenized_val = val_ds.map(preprocess_function)
tokenized_test = test_ds.map(preprocess_function)

tokenized_train.drop('text')
tokenized_val.drop('text')
tokenized_test.drop('text')



data_collator = DataCollatorWithPadding(tokenizer=tokenizer)


model = AutoModelForSequenceClassification.from_pretrained("mental/mental-bert-base-uncased", num_labels=3)
training_args = TrainingArguments(
    output_dir=f'./results/mentalbert/mental-transfer',
    per_device_train_batch_size=32,
    num_train_epochs=10,
    learning_rate=0.00005
)

# Trainer object initialization
trainer = Trainer(
    model=model,
    args=training_args,
    train_dataset=tokenized_train,
    tokenizer=tokenizer,
    data_collator=data_collator
)

trainer.train()

pred = trainer.predict(tokenized_val)

model_predictions = np.argmax(pred[0], axis=1)

model.save_pretrained(f"./models/mentalbert/mental-transfer-from-task2-3")
